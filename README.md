# parse-it

For simultaneously writing your parser and serializer.

Copies MegaParsec a lot.

## Status

Looking for feedback on the initial design particularly:
* [ParseNSerialize.PS](/src/ParseNSerialize.hs) type
* [ParseNSerialize.(<>>)](/src/ParseNSerialize.hs) "stacker" (not sure what to call it) that composes 2 `PS` types. It seems like it should be possible to get rid of this and use a more standard class if the `PS` type is modified.
* [ParseNSerialize.Stream.streamTransform](src/ParseNSerialize/Stream.hs) this is allows one parser serializer to accept another stream type (basically a lexer).
* The composability of the basic parser serializer builders in [ParseNSerialize.Stream.Builder](src/ParseNSerialize/Stream/Builder.hs)

## Intended Functionality

* simultaneously construct my parser and serializer at the same time
* stateful parser stream that can report the parse error position
* transformable stream so that a parser serializer can be written for one type of stream and then accept another

## Examples

[examples](/Examples.hs)

