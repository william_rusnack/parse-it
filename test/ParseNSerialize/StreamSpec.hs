module ParseNSerialize.StreamSpec where

import Data.Proxy
import qualified Data.Text as T
import Data.Text (Text)
import ParseNSerialize.Stream
import ParseNSerialize.Parse.Stream
import ParseNSerialize.Serialize.Stream
import ParseNSerialize.Stream.Builder
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Instances.Tagged ()
import Test.QuickCheck.Instances.Text ()
import Test.Types


spec :: Spec
spec = do
  describe "streamTransform" do
    it "parse" $ property $ \( text
                             , c
                             , applyFun3 -> modParsed
                             , initializer
                             ) -> do
      let ps :: PSStreamComplete Undefined Text GenericData GenericData2
          ps = single Proxy modParsed undefined c
      let psT :: PSStreamTransformedComplete Undefined String Text GenericData GenericData2
          psT = streamTransform T.pack undefined ps
      parse Proxy psT initializer (T.unpack text) `shouldBe` parse Proxy ps initializer text

    it "serialize" $ property $ \( gd
                                  , c
                                  , applyFun3 -> modToken
                                  ) -> do
      let ps :: PSStreamComplete Undefined Text Undefined GenericData
          ps = single Proxy undefined modToken c
      let serial = T.unpack $ serialize ps gd 
      let psT :: PSStreamTransformedComplete Undefined String Text Undefined GenericData
          psT = streamTransform undefined T.unpack ps
      let serialT :: String
          serialT = serialize psT gd
      serialT `shouldBe` serial

