module ParseNSerialize.Stream.File.LinesSpec where

-- import qualified Data.List.NonEmpty as NE
import Data.List as L
import Data.Proxy
import ParseNSerialize.Serialize.Stream
import ParseNSerialize.Stream
import ParseNSerialize.Stream.Builder
import ParseNSerialize.Stream.File
import ParseNSerialize.Stream.File.Lines as Lines
import ParseNSerialize.Utils
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Instances.Semigroup ()
import Test.QuickCheck.Instances.Tagged ()
import Test.Types
import Test.Arbitrary ()
import Debug.Trace
import ParseNSerialize.PS
import System.Timeout
import Control.Concurrent


spec :: Spec
spec = do
  xit "timeout" $ ioProperty $ timeout 100 do
    threadDelay 100000
    pure $ \(x :: Int, y) -> x `shouldBe` y

  describe "parse" do
    it "success" $ within 2000000 $ property $
      \( applyFun2 -> doesSatisfy :: Proxy (Lines String Char) -> String -> Bool
       , applyFun -> expand :: GenericData -> [GenericData1]
       , applyFun3 -> modToken :: Proxy (Lines String Char) -> GenericData1 -> String -> GenericData2
       , applyFun -> reduce :: [GenericData2] -> GenericData3
       , initializer
       , fp
       , getFileLines -> fileLines
       , renderEOL -> eol :: String
       ) -> do
        let ps :: FilePS Lines String Char GenericData GenericData3
            ps = many (satisfy Proxy modToken undefined doesSatisfy)
                      expand
                      reduce
                      undefined
        let tokens = mconcat $ (<> eol) <$> fileLines
        let expected :: Either FileError GenericData3
            expected = Right $
                       reduce $
                       fmap (uncurry $ modToken Proxy) $
                       takeWhile (doesSatisfy Proxy . snd) $
                       zip (expand initializer)
                           fileLines
        Lines.parse Proxy ps fp initializer tokens `shouldBe` expected

    fit "fail" $ ioProperty $ timeout 2000000 $ pure $
      \( -- fp
       applyFun -> expand :: () -> [()]
       , runInfFileLines -> fileLines
       -- , renderEOL -> eol
       ) -> do
        let fp = "fp"
        let eol = "\n"
        let ps :: FilePS Lines String Char () Undefined
            ps = many (anySingle Proxy (\_ _ t -> traceShow t ()) undefined)
                      expand
                      (const ())
                      undefined <>>
                 satisfy Proxy undefined undefined (const $ const False)
        let ln = L.length $ expand ()
        traceM $ "ln: " <> show ln
        let fi = sum $ (+ L.length eol) . L.length <$> take ln fileLines
        traceM $ "fi: " <> show fi
        let expected = Left $ FileError
              { errorPosition = Position
                { _filePath = fp
                , _fileIndex = FileIndex $ fi
                , _lineNumber = LineNumber ln
                , column = 1
                }
              , errorMsg = singleTokenDoesNotMatch
              }
        traceM "parse"
        let tokens = mconcat $ (<> eol) <$> fileLines
        Lines.parse Proxy ps fp () tokens `shouldBe` expected

  it "serialize" $ within 2000000 $ property $
    \( applyFun -> expand :: GenericData -> [GenericData1]
    , applyFun2 -> modExpanded :: Proxy (Lines String Char) -> GenericData1 -> Tokens (Lines String Char)
    , applyFun -> transformTokens :: Tokens (Lines String Char) -> Tokens String
    , toSerialize :: GenericData
    ) -> do
      let ps :: PSStreamComplete FileError (Lines String Char) Undefined GenericData
          ps = many (anySingle Proxy undefined modExpanded)
                    undefined
                    undefined
                    expand
      let psT :: PSStreamTransformedComplete FileError String (Lines String Char) Undefined GenericData
          psT = streamTransform undefined transformTokens ps
      let expected :: String
          expected = transformTokens . foldMap (modExpanded Proxy) $ expand toSerialize
      serialize psT toSerialize `shouldBe` expected

