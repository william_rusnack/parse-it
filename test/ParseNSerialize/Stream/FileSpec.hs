module ParseNSerialize.Stream.FileSpec where

import Data.List as L
import Data.Proxy
import ParseNSerialize.PS
import ParseNSerialize.Stream
import ParseNSerialize.Stream.Builder
import ParseNSerialize.Stream.File
import Safe
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Instances.Semigroup ()
import Test.QuickCheck.Instances.Tagged ()
import qualified Data.List.NonEmpty as NE


spec :: Spec
spec = do
  describe "File" do
    describe "parse" do
      it "error is correct" $ withMaxSuccess 1000 $ property $ \( tokens
                                           , NE.toList -> fp
                                           , NonNegative numSingles
                                           ) -> do
        let psMany :: FilePS File String Char Int ()
            psMany = many (anySingle Proxy (\_ _ _ -> ()) undefined)
                          (\x -> [1..x])
                          (const ())
                          undefined
        let ps = psMany <>> satisfy Proxy undefined undefined (\_ _ -> False)
        let len = L.length tokens
        let ls = L.lines tokens
        let numLs = case ls of [] -> 1
                               _ -> L.length ls
        let expected = Left $
              if | L.null tokens -> FileError
                   { errorPosition = Position
                     { _filePath = fp
                     , _fileIndex = 0
                     , _lineNumber = 1
                     , column = 1
                     }
                   , errorMsg = unexpectedEndOfStream
                   }
                 | len <= numSingles -> if lastMay tokens == pure '\n'
                   then FileError
                          { errorPosition = Position
                            { _filePath = fp
                            , _fileIndex = FileIndex len
                            , _lineNumber = LineNumber $ numLs + 1
                            , column = 1
                            }
                          -- , errorLine = ""
                          , errorMsg = unexpectedEndOfStream
                          }
                   else FileError
                          { errorPosition = Position
                            { _filePath = fp
                            , _fileIndex = FileIndex len
                            , _lineNumber = LineNumber $ numLs
                            , column = Column $ (+ 1) $ L.length $ lastDef mempty ls
                            }
                          -- , errorLine = ""
                          , errorMsg = unexpectedEndOfStream
                          }
                 | otherwise -> let ts = L.take numSingles tokens in FileError
                   { errorPosition = Position
                     { _filePath = fp
                     , _fileIndex = FileIndex numSingles
                     , _lineNumber = LineNumber $ (+ 1) $ L.length $ L.filter (== '\n') ts
                     , column = Column $ foldl' (\a x -> if x == '\n' then 1 else a + 1) 1 ts
                     }
                   -- , errorLine = ""
                   , errorMsg = singleTokenDoesNotMatch
                   }
        parse Proxy ps fp numSingles tokens `shouldBe` expected

