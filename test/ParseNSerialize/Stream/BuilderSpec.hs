module ParseNSerialize.Stream.BuilderSpec where

import Data.ByteString as B
import Data.Either
import Data.Foldable as F
import Data.List as L
import Data.Proxy
import Data.Word
import ParseNSerialize.PS
import ParseNSerialize.Parse.Stream
import ParseNSerialize.Serialize.Stream
import ParseNSerialize.Stream
import ParseNSerialize.Stream.Builder
import Test.Hspec
import Test.QuickCheck
import Test.QuickCheck.Instances.ByteString ()
import Test.QuickCheck.Instances.Tagged ()
import Test.Types


type PSSerialize = PSStreamComplete
  Undefined
  B.ByteString
  GenericData
  GenericData2
type PSParse = PSStreamComplete
  String
  B.ByteString
  GenericData
  GenericData2

spec :: Spec
spec = do
  describe "single" do
    describe "parse" do
      describe "succeed" do
        it "by itself" $ property $ \( preParse
                                     , applyFun3 -> modToken
                                     , BSNotNull tokens
                                     ) -> do
            let token = B.head tokens
            let ps :: PSParse
                ps = single Proxy
                            modToken
                            undefined
                            token
            let expected = Right $ modToken Proxy preParse token
            parse Proxy ps preParse tokens `shouldBe` expected

        it "stacked" $ property $ \( t
                                   , t'
                                   , applyFun3 -> modParsed :: Proxy B.ByteString -> GenericData -> Word8 -> GenericData1
                                   , applyFun3 -> modParsed'
                                   , initializer
                                   ) -> do
          let tokens = B.pack [t, t']
          let ps :: PSStreamComplete Undefined B.ByteString GenericData GenericData2
              ps = single Proxy modParsed  undefined t  <>> 
                   single Proxy modParsed' undefined t'
          let expected = Right $ modParsed' Proxy (modParsed Proxy initializer t) t'
          parse Proxy ps initializer tokens `shouldBe` expected

      describe "fail" do
        it "eof" $ property $ \token -> do
          let ps = single (Proxy :: Proxy B.ByteString)
                          undefined
                          undefined
                          token
          let expected = Left unexpectedEndOfStream :: Either String Undefined
          parse Proxy ps undefined mempty `shouldBe` expected

        it "wrong char" $ forAll (suchThat arbitrary (uncurry (/=))) $ \(t, t') -> do
          let ps = single (Proxy :: Proxy B.ByteString)
                          undefined
                          undefined
                          t
          let expected = Left singleTokenDoesNotMatch :: Either String Undefined
          parse Proxy ps undefined (B.singleton t') `shouldBe` expected

        it "stacked wrong char" $ property $ \( pairs
                                              , initializer
                                              ) -> do
          let OnePairNotEqual (x, x') (y, y') = pairs
          let tokens = B.pack [x', y']
          let ps :: PSStreamComplete String B.ByteString GenericData GenericData2
              ps = single Proxy undefined undefined x  <>> 
                   single Proxy undefined undefined y
          let expected = Left singleTokenDoesNotMatch
          parse Proxy ps initializer tokens `shouldBe` expected

    describe "serialize" do
      it "by itself" $ property $ \(token, applyFun3 -> modToken, toSerialize) -> do
        let ps :: PSSerialize
            ps = single Proxy
                        undefined
                        modToken
                        token
        let expected = modToken Proxy toSerialize token
        serialize ps toSerialize `shouldBe` expected

      it "stacked" $ property $ \( t
                                 , t'
                                 , applyFun3 -> modToken
                                 , applyFun3 -> modToken'
                                 , toSerialize
                                 ) -> do
        let ps :: PSSerialize
            ps = single Proxy undefined modToken  t  <>>
                 single Proxy undefined modToken' t'
        let expected :: ByteString
            expected = modToken Proxy toSerialize t <> modToken' Proxy toSerialize t'
        serialize ps toSerialize `shouldBe` expected

  describe "chunk" do
    describe "parse" do
      it "success" $ property $ \( BSNotNull chunkTokens
                                 , excessTokens
                                 , applyFun3 -> modParsed
                                 , initializer
                                 ) -> do
        let ps :: PSStreamComplete Undefined B.ByteString GenericData GenericData1
            ps = chunk Proxy modParsed undefined chunkTokens
        let expected = Right $ modParsed Proxy initializer chunkTokens
        parse Proxy ps initializer (chunkTokens <> excessTokens) `shouldBe` expected

      it "fail" $ forAll ( suchThat arbitrary $
        \(BSNotNull cts, BSNotNull ts) -> not $ cts `B.isPrefixOf` ts ) $
        \(BSNotNull chunkTokens, BSNotNull tokens ) -> do
          let ps :: PSStreamComplete String B.ByteString Undefined Undefined
              ps = chunk Proxy undefined undefined chunkTokens
          let expected = Left chunksDoNotMatch
          parse Proxy ps undefined tokens `shouldBe` expected

    it "serialize" $ property $ \( BSNotNull chunkTokens
                                 , applyFun3 -> modTokens
                                 , toSerialize
                                 ) -> do
      let ps :: PSSerialize
          ps = chunk Proxy undefined modTokens chunkTokens
      let expected = modTokens Proxy toSerialize chunkTokens
      serialize ps toSerialize `shouldBe` expected

  describe "many" do
    describe "parse" do
      it "matches" $ property $
        \( initializer
         , applyFun3 -> modToken
         , applyFun3 -> modTokens
         , applyFun -> expandParse
         , applyFun -> reduceParse
         -- , applyFun -> expandSerial
         , singleTokens
         , BSNotNull chunkTokens
         ) -> do
          let psAS :: PSStream Undefined B.ByteString GenericData2 GenericData3 GenericData4
              psAS = anySingle Proxy modToken undefined
          let psChunk :: PSStream Undefined B.ByteString GenericData3 GenericData4 GenericData4
              psChunk = chunk Proxy modTokens undefined chunkTokens
          let psSub :: PSStreamComplete Undefined B.ByteString GenericData2 GenericData4
              psSub = psAS <>> psChunk
          let ps :: PSStreamComplete Undefined B.ByteString GenericData GenericData1
              ps = many psSub expandParse reduceParse undefined
          let expected = Right $ reduceParse $ fmap
                (\(t, pre) -> let anySingleResult = modToken Proxy pre t
                       in modTokens Proxy anySingleResult chunkTokens) $
                L.zip singleTokens (expandParse initializer)
          let stream = F.foldr (\t s -> cons t chunkTokens <> s) mempty singleTokens 
          forAll (suchThat arbitrary $ isLeft . parse Proxy psSub undefined) $ \excessTokens ->
            parse Proxy ps initializer (stream <> excessTokens) `shouldBe` expected

    it "serialize" $ property $
      \( BSNotNull chunkTokens
       , applyFun3 -> serializeTokens
       , applyFun -> expandSerial
       , toSerialize
       ) -> do
        let psChunk :: PSStreamComplete Undefined B.ByteString Undefined GenericData
            psChunk = chunk Proxy undefined serializeTokens chunkTokens
        let ps :: PSStreamComplete Undefined B.ByteString Undefined GenericData1
            ps = many psChunk undefined undefined expandSerial
        let expected = foldMap (\gd -> serializeTokens Proxy gd chunkTokens) $ expandSerial toSerialize
        serialize ps toSerialize `shouldBe` expected

