{-# OPTIONS_GHC -Wwarn #-}

module Test.Arbitrary where

import ParseNSerialize.Utils
import Test.QuickCheck


instance Arbitrary EOL where
  arbitrary = elements [UnixEOL, WindowsEOL]
  shrink WindowsEOL = [UnixEOL]
  shrink UnixEOL = []

