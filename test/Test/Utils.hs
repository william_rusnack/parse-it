module Test.Utils where

accum :: Monoid a => [a] -> [a]
accum = f mempty
  where f a (m:ms) = a' : f a' ms
          where a' = m <> a
        f _ [] = []
