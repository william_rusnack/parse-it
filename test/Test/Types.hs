{-# LANGUAGE EmptyDataDeriving, DeriveAnyClass #-}

module Test.Types where

import Data.ByteString as B
import Data.List as L
import Data.Text as T
import ParseNSerialize.Stream
import Test.QuickCheck
import Test.QuickCheck.Instances.ByteString ()
import GHC.Generics

data OnePairNotEqual a = OnePairNotEqual (a, a) (a, a) deriving Show
instance (Arbitrary a, Eq a) => Arbitrary (OnePairNotEqual a) where
  arbitrary = do
    pair <- (\x -> (x, x)) <$> arbitrary
    pair' <- suchThat arbitrary (uncurry (/=))
    elements [OnePairNotEqual pair pair', OnePairNotEqual pair' pair]

data Undefined deriving (Show, Eq)
instance ParseStreamFail String Undefined where parseStreamFail = undefined
instance ParseStreamFail ByteString Undefined where parseStreamFail = undefined
instance ParseStreamFail Text Undefined where parseStreamFail = undefined

newtype GenericData = GenericData Word
  deriving stock (Show, Eq, Ord, Generic)
  deriving newtype (Num, Enum, Real, Integral, Arbitrary, CoArbitrary)
  deriving anyclass (Function)
-- instance Function GenericData where function = functionIntegral

newtype GenericData1 = GenericData1 Word
  deriving stock (Show, Eq, Ord, Generic)
  deriving newtype (Num, Enum, Real, Integral, Arbitrary, CoArbitrary)
  deriving anyclass (Function)
-- instance Function GenericData1 where function = functionIntegral

newtype GenericData2 = GenericData2 Word
  deriving stock (Show, Eq, Ord, Generic)
  deriving newtype (Num, Enum, Real, Integral, Arbitrary, CoArbitrary)
  deriving anyclass (Function)
-- instance Function GenericData2 where function = functionIntegral

newtype GenericData3 = GenericData3 Word
  deriving stock (Show, Eq, Ord, Generic)
  deriving newtype (Num, Enum, Real, Integral, Arbitrary, CoArbitrary)
  deriving anyclass (Function)
-- instance Function GenericData3 where function = functionIntegral

newtype GenericData4 = GenericData4 Word
  deriving stock (Show, Eq, Ord, Generic)
  deriving newtype (Num, Enum, Real, Integral, Arbitrary, CoArbitrary)
  deriving anyclass (Function)
-- instance Function GenericData4 where function = functionIntegral

newtype BSNotNull = BSNotNull B.ByteString deriving Show
instance Arbitrary BSNotNull where
  arbitrary = BSNotNull <$> suchThat arbitrary (not . B.null)
  shrink (BSNotNull bs) = BSNotNull <$> L.filter (not . B.null) (shrink bs)

newtype FileLines = FileLines {getFileLines :: [String]} deriving Show
instance Arbitrary FileLines where
  arbitrary = FileLines . fmap (L.filter (/= '\n')) <$> arbitrary
  shrink = fmap FileLines . shrink . getFileLines

newtype InfFileLines = InfFileLines {getInfFileLines :: InfiniteList String}
instance Show InfFileLines where
  show il = "InfFileLines (" <> show il <> ")"
instance Arbitrary InfFileLines where
  arbitrary = InfFileLines .
              (\(InfiniteList l i) -> InfiniteList (fmap (L.filter (/= '\n')) l) i) <$>
              arbitrary
  shrink = fmap InfFileLines . shrink . getInfFileLines
runInfFileLines :: InfFileLines -> [String]
runInfFileLines = getInfiniteList . getInfFileLines

