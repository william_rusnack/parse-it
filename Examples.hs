module Examples where

import Data.Either.Combinators
import Data.Functor.Identity
import Data.List as L
import Data.Proxy
import Data.Text as T
import Parse.Stream.Generic (parse, parseRecord)
import ParseNSerialize ((<>>))
import ParseNSerialize.Stream (PSStreamComplete, PSStreamTransformedComplete, PSStreamRecord, streamTransform)
import ParseNSerialize.Stream.Builder (single)
import Safe
import Serialize.Stream (serialize)
import Stream
import Utils (sshow)


-- ParseNSerialize Record --

data Letters = Letters {firstLetter :: Char, secondLetter :: Char} 

-- I only have `single` working right now
psL :: PSStreamComplete String Text Letters Letters
psL =
  single Proxy (\_ r t -> r {firstLetter = t}) (\_ r _ -> T.singleton $ firstLetter r) 'f' <>>
  single Proxy (\_ r t -> r {firstLetter = t}) (\_ r _ -> T.singleton $ firstLetter r) 's'

textToParse :: Text
textToParse = "fs"

preParseRecordL :: Letters
preParseRecordL = Letters { firstLetter = '0'
                         , secondLetter = '0'
                         }

parseResultL :: Either String Letters
parseResultL = parse Proxy psL preParseRecordL textToParse

serializeResultL :: Text
serializeResultL = serialize psL (Letters 'f' 's')


-- ParseNSerialize Record --

data Numbers n = Numbers {numerator :: n Int, denominator :: n Int}

-- this needs to be improved. perhaps with Traversable
resolveNumbers :: Numbers (Either a) -> Either a (Numbers Identity)
resolveNumbers (Numbers (Left l) _) = Left l
resolveNumbers (Numbers _ (Left l)) = Left l
resolveNumbers (Numbers (Right n) (Right d)) = Right $ Numbers (Identity n) (Identity d)

psN :: PSStreamRecord String Text Numbers (Either String)
psN =
  single Proxy
         (\_ r t -> r { numerator = maybeToRight "Could not convert numerator to Int"
                                                 (readMay $ pure t) })
         (\_ r _ -> sshow $ runIdentity $ numerator r)
         '1' <>>
  single Proxy (\_ r _ -> r) (\_ _ _ -> "/") '/' <>> -- there should be a shortend version of single for this case
  single Proxy
         (\_ r t -> r { denominator = maybeToRight "Could not convert denominator to Int"
                                                   (readMay $ pure t) })
         (\_ r _ -> sshow $ runIdentity $ denominator r)
         '5'

preParseRecordN :: Numbers (Either String)
preParseRecordN = Numbers { numerator = Left "uninitialized numerator"
                          , denominator = Left "uninitialized denominator"
                          }

parseResultN :: Either String (Numbers Identity)
parseResultN = parseRecord Proxy psN resolveNumbers preParseRecordN "1/5"

toSerializeN :: Numbers Identity
toSerializeN = Numbers { numerator = Identity 20
                       , denominator = Identity 40
                       }

serializeResultN :: Text
serializeResultN = serialize psN toSerializeN


-- Parse with function. Serialize Record --

data Things = Things {thing1 :: Int, thing2 :: Char}

psT :: PSStreamComplete String Text (Int -> Char -> Things) Things
psT = single Proxy (\_ constructor _ -> constructor 1) (\_ r _ -> sshow $ thing1 r) '1' <>> -- there should be a shortend version of single for this case
      single Proxy (\_ constructor t -> constructor t) (\_ r _ -> T.singleton $ thing2 r) '2'

parseResultT :: Either String Things
parseResultT = parse Proxy psT Things "12"

serializeResultT :: Text
serializeResultT = serialize psT (Things 1 '2')


-- Stream Transform or Lexer --

data LearnToCount = LearnToCount Double Double Double

type LTC = Double -> Double -> Double -> LearnToCount

newtype DoubleStream = DoubleStream [Double]

instance Stream DoubleStream where
  type Token DoubleStream = Double
  type Tokens DoubleStream = [Double]
  take1_ (DoubleStream (d:ds)) = Right (d, DoubleStream ds)
  take1_ (DoubleStream []) = Left unexpectedEndOfStream
  tokenize _ = id
  streamize _ = DoubleStream

instance ParseStreamFail DoubleStream String where
  parseStreamFail msg _ = msg


psLTC :: PSStreamComplete String DoubleStream LTC LearnToCount
psLTC = single Proxy (\_ constructor t -> constructor t) (\_ (LearnToCount x _ _) _ -> pure x) 1.0 <>>
        single Proxy (\_ constructor t -> constructor t) (\_ (LearnToCount _ x _) _ -> pure x) 2.0 <>>
        single Proxy (\_ constructor t -> constructor t) (\_ (LearnToCount _ _ x) _ -> pure x) 3.0

-- there should be a way to throw an error if the stream is not transformed correctly
-- converting to the new stream tokens needs to allow for seperators
psLTC' :: PSStreamTransformedComplete String String DoubleStream LTC LearnToCount
psLTC' = streamTransform (DoubleStream . fmap read . L.words)
                         (show)
                         psLTC

parseResultStreamTransform :: Either String LearnToCount
parseResultStreamTransform = parse
  Proxy
  psLTC'
  LearnToCount
  ("1.0 2.0 3.0" :: String)


-- Sum Type --

data SumType a b c = ST0 a | ST1 b | ST2 c

type ST = SumType Int Integer Double

psST :: PSStreamComplete String Text () ST
psST =
  single -- should use anySingle but I have not implemented that yet (sorry)
    Proxy
    (\_ () t -> case t of 'I' -> ST0 . read
                          'L' -> ST1 . read
                          'D' -> ST2 . read
                          _ -> undefined ) -- anySingle will allow an error to be thrown
    (\_ sumType _ -> case sumType of ST0 _ -> "I"
                                     ST1 _ -> "L"
                                     ST2 _ -> "D" )
    'I' <>> -- because anySingle is not implemented
  single -- should be many but not implemented yet
    Proxy
    (\_ c t -> let ts = [t] in c ts) -- with many t should actaully be ts
    (\_ sumType _ -> case sumType of ST0 x -> sshow x
                                     ST1 x -> sshow x
                                     ST2 x -> sshow x )
    '1' -- because anySingle is not implemented

parseResultST :: Either String ST
parseResultST = parse Proxy psST () "I1"

serializeResultST :: Text
serializeResultST = serialize psST (ST1 5)

main :: IO ()
main = undefined

