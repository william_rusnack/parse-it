module ParseNSerialize.Stream.File.Lines where

import Data.Proxy
import Lens.Micro
import Lens.Micro.TH
import ParseNSerialize.PS
import ParseNSerialize.Stream
import ParseNSerialize.Stream.File
import Data.String
import qualified Data.ByteString as B
import qualified Data.Text as T
import ParseNSerialize.Utils as Utils
import ParseNSerialize.Stream.Builder
import Control.Arrow
import Data.Functor.Identity


data Lines string char = Lines
  { _lPosition :: Position
  , lines :: [string]
  }
makeLenses ''Lines
instance Length string => Stream (Lines string char) where
  type Token (Lines string char) = string
  type Tokens (Lines string char) = [string]
  take1_ (Lines p (l:ls)) = Right (l, Lines newPositon ls)
    where newPositon = p & lineNumber +~ 1
                         & fileIndex +~ FileIndex (Utils.length l)
  take1_ (Lines _ []) = Left unexpectedEndOfStream
  pack _ = id
  tokenize = undefined
  streamize = undefined
instance PositionLens (Lines string char) where position = lPosition

class DetectEOL a where
  detectEOL :: a -> EOL

instance (Eq a, EolChars a) => DetectEOL [a] where
  detectEOL (x:xs@(y:_)) | x == newline = UnixEOL
                         | x == carriageReturn && y == newline = WindowsEOL
                         | otherwise = detectEOL xs
  detectEOL _ = UnixEOL

instance DetectEOL T.Text where detectEOL = detectEOL . T.unpack
instance DetectEOL B.ByteString where detectEOL = detectEOL . B.unpack

-- class SplitLines a b where splitLines :: Proxy b -> a -> a -> [a]
-- 
-- instance Eq a => SplitLines [a] a where
--  lsplitLines _ _ [] = []
--   splitLines _ eol stream = 
--     where sl :: [a] -> [[a]]
--           sl s = case stripPrefix eol s of
--             Just s' 
--             Nothing

instance ParseStreamFail (Lines string char) FileError where
  parseStreamFail msg file = FileError
    { errorPosition = file ^. position
    , errorMsg = msg
    }

lowerLines :: forall string char preParse a.
              ( Token (Lines string char) ~ Tokens string
              , Eq (Token string)
              , Monoid string
              , IsString string
              , Tokens string ~ string
              , Stream string
              )
           => FilePath
           -> EOL
           -> FilePS Lines string char preParse a
           -> PSStreamTransformedComplete FileError string (Lines string char) preParse a
lowerLines fp (renderEOL -> eol) = streamTransform
    ( ( (() ,) >>>
        getParser ps
      ) :: string -> Lines string char)
    undefined
  where ps :: PS ((), string) (Lines string char) (Tokens (Lines string char)) string Identity
        ps = many ( manyTill (anySingle Proxy (\_ _ t -> t) undefined)
                             (chunk Proxy (\_ _ _ -> ()) (\_ _ _ -> eol) eol)
                             (const $ repeat ())
                             (const ())
                             (\(ts :: [Token string]) _ -> pack (Proxy :: Proxy string) ts)
                             undefined
                             undefined )
                  (const $ repeat ())
                  positionedLines
                  undefined
        positionedLines :: [string] -> Lines string char
        positionedLines = Lines $ defaultPosition & filePath .~ fp

parse :: ( Stream string
         , DetectEOL string
         -- , SplitLines string char
         , Monoid string
         , IsString string
         , IncrementPosition char
         , Tokens string ~ string
         , Token string ~ char
         , PositionLens (Lines string char)
         , Eq char
         )
      => Proxy (Lines string char)
      -> FilePS Lines string char preParse a
      -> FilePath
      -> preParse
      -> Tokens string
      -> Either FileError a
parse _ ps fp pre tokens = fst <$> parser (pre, tokens)
  where parser = getParser $ lowerLines fp eol ps
        eol = detectEOL tokens

