module ParseNSerialize.Stream.File where

import Data.Functor
import Data.Proxy
import Data.Word
import Lens.Micro.TH
import ParseNSerialize.PS
import ParseNSerialize.Stream


newtype FileIndex = FileIndex Int deriving (Show, Eq, Num)
newtype LineNumber = LineNumber Int deriving (Show, Eq, Num)
newtype Column = Column Int deriving (Show, Eq, Num)

data Position = Position
  { _filePath :: FilePath
  , _fileIndex :: FileIndex
  , _lineNumber :: LineNumber
  , column :: Column
  } deriving (Show, Eq)

makeLenses ''Position

defaultPosition :: Position
defaultPosition = Position {_filePath = "", _fileIndex = 0, _lineNumber = 1, column = 1}

class IncrementPosition token where
  incrementPosition :: token -> Position -> Position

class PositionLens a where
  position :: Functor f => (Position -> f Position) -> a -> f a

data File string char = File
  { _fPosition :: Position
  , fString :: string
  } deriving Show
makeLenses ''File
instance (Stream string, IncrementPosition char, Tokens string ~ string, Token string ~ char) => Stream (File string char) where
  type Token  (File string char) = char
  type Tokens (File string char) = Tokens string
  take1_ (File p s) = take1_ s <&> \(t, s') -> (t, File (incrementPosition t p) s')
  pack = pack
  tokenize _ = tokenize (Proxy :: Proxy string)
  streamize _ = File defaultPosition
instance PositionLens (File string char) where position = fPosition

instance IncrementPosition Word8 where
  -- https://www.asciitable.com/
  -- \n 10
  incrementPosition 10 (Position fp i ln _) = Position fp (i + 1) (ln + 1) 1
  -- -- \r 13
  -- incrementPosition 13 (Position fp i ln c) = Position fp (i + 1) ln c
  incrementPosition _ (Position fp i ln c) = Position fp (i + 1) ln (c + 1)

instance IncrementPosition Char where
  -- https://www.asciitable.com/
  incrementPosition '\n' (Position fp i ln _) = Position fp (i + 1) (ln + 1) 1
  -- incrementPosition '\r' (Position fp i ln c) = Position fp (i + 1) ln c
  incrementPosition _ (Position fp i ln c) = Position fp (i + 1) ln (c + 1)



type FilePS stream string char preParse a = PSStreamComplete
  FileError (stream string char) preParse a

data FileError = FileError
  { errorPosition :: Position
  -- , errorLine :: String
  , errorMsg :: String
  } deriving (Show, Eq)

instance ParseStreamFail (File string char) FileError where
  parseStreamFail msg file = FileError
    { errorPosition = _fPosition file
    -- , errorLine = "currently not setting error line in ParseStreamFail"
    , errorMsg = msg
    }

lowerFile :: FilePath
          -> FilePS File string char preParse a
          -> PSStreamTransformedComplete FileError string (File string char) preParse a
lowerFile fp = streamTransform (File defaultPosition {_filePath = fp})
                               id


parse :: ( Stream string
         , IncrementPosition char
         , Tokens string ~ string
         , Token string ~ char
         , PositionLens (File string char)
         )
      => Proxy (File string char)
      -> FilePS File string char preParse a
      -> FilePath
      -> preParse
      -> Tokens string
      -> Either FileError a
parse _ ps fp pre tokens = fst <$> parser (pre, tokens)
  where parser = getParser $ lowerFile fp ps
-- parse prx (PS p _) fp preParse tokens = fst <$> p (preParse, stream)
--   where stream = streamize prx tokens & position . filePath .~ fp

