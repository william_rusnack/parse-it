module ParseNSerialize.Stream.Builder
  ( satisfy
  , anySingle
  , single
  , singleTokenDoesNotMatch
  , chunk
  , chunksDoNotMatch
  , many
  , manyTill
  ) where

import Control.Arrow ((>>>))
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Writer
import Data.Bifunctor
import Data.Proxy
import ParseNSerialize.PS
import ParseNSerialize.Stream
import Data.Foldable
import Lens.Micro


satisfy :: (Stream stream, ParseStreamFail stream error)
        => Proxy stream
        -> (Proxy stream -> preParse -> Token stream -> parsed)
        -> (Proxy stream -> a -> Tokens stream)
        -> (Proxy stream -> Token stream -> Bool)
        -> PSStream error stream preParse parsed a
satisfy prx combine serialize test = PS p s
  where p (pre, stream) = do
          (t, stream') <- first (\msg -> parseStreamFail msg stream) $
                           take1_ stream
          if test prx t
            then pure (combine prx pre t, stream')
            else Left $ parseStreamFail singleTokenDoesNotMatch stream
        s = ReaderT $ tell . serialize prx

singleTokenDoesNotMatch :: String
singleTokenDoesNotMatch = "single token does not match"

single :: (Stream stream, ParseStreamFail stream error, Eq (Token stream))
       => Proxy stream
       -> (Proxy stream -> preParse -> Token stream -> parsed)
       -> (Proxy stream -> a -> Token stream -> Tokens stream)
       -> Token stream
       -> PSStream error stream preParse parsed a
single prx combine serialize t = satisfy
  prx
  combine
  (\_ preParse -> serialize prx preParse t)
  (const (== t))

anySingle :: (Stream stream, ParseStreamFail stream error)
          => Proxy stream
          -> (Proxy stream -> preParse -> Token stream -> parsed)
          -> (Proxy stream -> a -> Tokens stream)
          -> PSStream error stream preParse parsed a
anySingle prx combine serialize = satisfy prx combine serialize (const $ const True)

chunk :: (Stream stream, ParseStreamFail stream error, Eq (Token stream))
      => Proxy stream
      -> (Proxy stream -> preParse -> Tokens stream -> parsed)
      -> (Proxy stream -> a -> Tokens stream -> Tokens stream)
      -> Tokens stream
      -> PSStream error stream preParse parsed a
chunk prx combine serialize ts = PS p s
  where p (pre, stream) = bimap
          (\msg -> parseStreamFail msg stream)
          (combine prx pre ts,) $
          foldEither fe (tokenize prx ts) stream
        fe t stream = case take1_ stream of
          Right (t', stream') | t == t' -> pure stream'
                              | otherwise -> Left chunksDoNotMatch
          Left x -> Left x
        s = ReaderT $ tell . \x -> serialize prx x ts

chunksDoNotMatch :: String
chunksDoNotMatch = "chunks do not match"

many :: (Stream stream, Monoid (Tokens stream))
     => PSStream error stream firstParsed secondParsed b
     -> (preParse -> [firstParsed])
     -> ([secondParsed] -> parsed)
     -> (a -> [b])
     -> PSStream error stream preParse parsed a
many (PS p s) expandParse reduceParse expandSerial = PS
  (first expandParse >>> p' >>> first reduceParse >>> Right)
  (ReaderT $ expandSerial >>> traverse_ (runReaderT s))
  where p' (preParse:ps, stream) = case p (preParse, stream) of
          Right (parsed, stream') -> first (parsed :) $ p' (ps, stream')
          Left _ -> ([], stream)
        p' ([], stream) = ([], stream)

manyTill :: PS preMany postMany b serialized
         -> (preParse -> [preMany])
         -> ([postMany] -> postTill -> parsed)
         -> (a -> [b])
         -> PS preTill postTill c serialized
         -> (preParse -> preTill)
         -> (a -> c)
         -> PS preParse parsed a serialize
manyTill (PS p s) expandParse reduce expandSerial (PS p' s') toTill tillSerial = PS
  ( (expandParse &&& toTill) >>>
    (\(ep, tl) -> 
  ( ReaderT $ \r -> do
      traverse_ (runReaderT s) (expandSerial r) 
      runReaderT s' $ tillSerial r )

manyTill :: forall error stream preParse firstParsed preTill secondParsed tillParsed parsed a b c.
         ( ParseStreamFail stream error
         , Monoid (Tokens stream)
         )
         => PSStream error stream firstParsed secondParsed b
         -> PSStream error stream preTill tillParsed c
         -> (preParse -> [firstParsed])
         -> (preParse -> preTill)
         -> ([secondParsed] -> tillParsed -> parsed)
         -> (a -> [b])
         -> (a -> c)
         -> PSStream error stream preParse parsed a
manyTill (PS p s) (PS p' s') expandParse tillParse reduce expandSerial tillSerial = PS
  parse
  ( ReaderT $ \r -> do
      traverse_ (runReaderT s) (expandSerial r) 
      runReaderT s' $ tillSerial r )
  where parse (preParse, initStream) = case mt (expandParse preParse) initStream of
          Right (ss, tp, stream') -> Right (reduce ss tp, stream')
          Left e -> Left e
          where mt :: [firstParsed] -> stream -> Either error ([secondParsed], tillParsed, stream)
                mt (f:fs) stream = case p' (tillParse preParse, stream) of
                  Right (tp, stream') -> Right ([], tp, stream')
                  Left _ -> case p (f, stream) of
                    Right (x, stream') -> mt fs stream' & _Right . _1 %~ (x :)
                    Left _ -> Left $ parseStreamFail "Did not meet end condition before the many match failed with manyTill" stream
                mt [] stream = Left $ parseStreamFail "Did not find end condidtion before expanded [firstParsed] emptied" stream

-- unfoldStream :: Stream stream => stream -> [Either String (Token stream, stream)]
-- unfoldStream = uf . take1_
--   where uf r@(Right (_, s)) = r : uf (take1_ s)
--         uf l = repeat l

foldEither :: Foldable f => (a -> b -> Either c b) -> f a -> b -> Either c b
foldEither eitherFunction foldValues initializer = f (toList foldValues) (pure initializer)
  where f (x:xs) (Right y)   = f xs (eitherFunction x y)
        f _      l@(Left _)  = l
        f []     e           = e

