module ParseNSerialize.Parse.Stream where

import Control.Monad
import Data.Functor.Identity
import Data.Proxy
import ParseNSerialize.PS
import ParseNSerialize.Stream


parse :: Stream stream
      => Proxy stream
      -> PSStreamTransformedComplete error stream rootStream preParse a
      -> preParse
      -> Tokens stream
      -> Either error a
parse prx (PS p _) preParse tokens = fst <$> p (preParse, streamize prx tokens)

parseRecord :: Stream stream
            => Proxy stream
            -> PSStreamTransformed error stream rootStream (record m) (record m) (record Identity)
            -> (record m -> Either error (record Identity))
            -> record m
            -> Tokens stream
            -> Either error (record Identity)
parseRecord prx (PS p _) transform r tokens = join $ transform . fst <$> p (r, streamize prx tokens)

