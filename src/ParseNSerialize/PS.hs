module ParseNSerialize.PS where

import Control.Monad
import Control.Monad.Trans.Reader
import Control.Monad.Trans.Writer


-- data PS error preParse parsed a serialized = PS
--   { getParser :: (preParse -> Either error parsed)
--   , getSerializer :: (ReaderT a (Writer serialized) ())
--   }

data PS preParse parsed a serialized m = PS
  { getParser :: preParse -> m parsed
  , getSerializer :: ReaderT a (Writer serialized) ()
  }

type PSFiniteComplete error preParse parsed serialized = PS preParse parsed parsed serialized (Either error)

(<>>) :: (Monad m, Monoid serialized)
      => PS preParse midParse a serialized m
      -> PS midParse parsed   a serialized m
      -> PS preParse parsed   a serialized m
(PS p s) <>> (PS p' s') = PS (p >=> p') (s >>= const s')

