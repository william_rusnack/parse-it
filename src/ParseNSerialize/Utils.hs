module ParseNSerialize.Utils where

import Data.String
import Data.Word
import Data.Foldable as F
import Debug.Trace


sshow :: (Show a, IsString s) => a -> s
sshow = fromString . show

-- https://github.com/ekmett/either/blob/4c86ff3f9b27795f588d207e09de14b11ae771f4/src/Data/Either/Combinators.hs#L332
maybeToRight :: b -> Maybe a -> Either b a
maybeToRight _ (Just x) = Right x
maybeToRight y Nothing  = Left y

class Length a where length :: a -> Int
instance Length [a] where length = F.length

data EOL = UnixEOL | WindowsEOL deriving Show

renderEOL :: IsString a => EOL -> a
renderEOL UnixEOL = "\n"
renderEOL WindowsEOL = "\r\n"

class EolChars a where
  carriageReturn :: a
  newline :: a

instance EolChars Char where
  carriageReturn = '\r'
  newline = '\n'

instance EolChars Word8 where
  carriageReturn = 13
  newline = 10

traceMsgShow :: Show a => String -> a -> a
traceMsgShow msg v = trace (msg <> ": " <> show v) v

