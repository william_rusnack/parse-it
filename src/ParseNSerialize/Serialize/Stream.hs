module ParseNSerialize.Serialize.Stream where

import Control.Monad.Trans.Reader
import Control.Monad.Trans.Writer
import ParseNSerialize.PS
import ParseNSerialize.Stream


serialize :: PSStreamTransformed error stream rootStream preParse parsed toSerialize
          -> toSerialize
          -> Tokens stream
serialize (PS _ s) x = execWriter $ runReaderT s x

