module ParseNSerialize.Stream where

import Control.Monad.Trans.Reader
import Control.Monad.Trans.Writer
import Data.Bifunctor
import Data.Functor.Identity
import Data.Proxy
import Data.Word
import ParseNSerialize.Utils
import ParseNSerialize.PS
import qualified Data.ByteString as B
import qualified Data.List as L
import qualified Data.Text as T


class Stream stream where
  type Token stream :: *
  type Tokens stream :: * 
  take1_ :: stream -> Either String (Token stream, stream)
  -- takeN_ :: Int -> stream -> Either String (Tokens stream, stream)
  -- takeWhile_ :: (Token stream -> Bool) -> stream -> (Tokens stream, stream)
  -- singleton :: Proxy stream -> Token stream -> Tokens stream
  -- append :: Proxy stream -> Tokens stream -> Token stream -> Tokens stream
  pack :: Proxy stream -> [Token stream] -> Tokens stream
  tokenize :: Proxy stream -> Tokens stream -> [Token stream]
  streamize :: Proxy stream -> Tokens stream -> stream

class ParseStreamFail stream error where
  parseStreamFail :: String -> stream -> error

type PSStream error stream preParse parsed a = PSStreamTransformed
  error stream stream preParse parsed a

type PSStreamComplete error stream preParse a = PSStream
  error stream preParse a a

type PSStreamRecord error stream record m = PSStreamTransformed
  error stream stream (record m) (record m) (record Identity)

type PSStreamTransformed error stream rootStream preParse parsed a = PS
  (preParse, stream)
  (parsed, rootStream)
  a
  (Tokens stream)
  (Either error) 

type PSStreamTransformedComplete error stream rootStream preParse a =
  PSStreamTransformed error stream rootStream preParse a a

streamTransform :: (stream -> origStream)
                -> (Tokens origStream -> Tokens stream)
                -> PSStreamTransformedComplete error origStream rootStream preParse a
                -> PSStreamTransformedComplete error stream     rootStream preParse a
streamTransform ms mt (PS p s) = PS modP modS
  where modP (preParse, stream) = case p (preParse, ms stream) of
          Right x -> Right x
          Left e -> Left e
        modS = mapReaderT (mapWriter (second mt)) s

-- Error Messages --

unexpectedEndOfStream :: String
unexpectedEndOfStream = "Unexpected end of stream."

-- Instances --

instance ParseStreamFail stream () where
  parseStreamFail _ _ = ()

instance Stream String where
  type Token String = Char
  type Tokens String = String
  take1_ = maybeToRight unexpectedEndOfStream . L.uncons
  pack _ = id
  tokenize _ = id
  streamize _ = id

instance Stream B.ByteString where
  type Token B.ByteString = Word8
  type Tokens B.ByteString = B.ByteString
  take1_ = maybeToRight unexpectedEndOfStream . B.uncons
  -- takeN_ n s
  --   | n <= 0    = pure (B.empty, s)
  --   | B.null s  = Left unexpectedEndOfStream
  --   | otherwise = pure (B.splitAt n s)
  -- takeWhile_ = B.span
  -- singleton _ = B.singleton
  pack _ = B.pack
  tokenize _ = B.unpack
  streamize _ = id
instance ParseStreamFail B.ByteString String where parseStreamFail e _ = e

instance Stream T.Text where
  type Token T.Text = Char
  type Tokens T.Text = T.Text
  take1_ = maybeToRight unexpectedEndOfStream . T.uncons
  -- takeN_ n s
  --   | n <= 0    = pure (T.empty, s)
  --   | T.null s  = Left unexpectedEndOfStream
  --   | otherwise = pure (T.splitAt n s)
  -- takeWhile_ = T.span
  -- singleton _ = T.singleton
  pack _ = T.pack
  tokenize _ = T.unpack
  streamize _ = id
instance ParseStreamFail T.Text String where parseStreamFail e _ = e


